package com.example.design.pattern.creational;

/**
 * 懒汉式单例：只在第一次使用单例类创建单例类，避免创建重量级单例类慢的问题。在高并发情况下，同步关键字的使用可能会降低性能
 * 
 * @author wangpeng
 *
 */
public class LazySingleton {

	private LazySingleton() {
		System.out.println("LazySingleton is create");
	}

	// public static int staticMember = 0;

	private static LazySingleton instance = null;

	public static synchronized LazySingleton getInstance() {
		if (instance == null) {
			instance = new LazySingleton();
		}
		return instance;
	}

	// public static void createString() {
	// System.out.println("createString in Singleton");
	// }

	// public static void main(String[] args) {
	// // 只输出createString in Singleton
	// LazySingleton.staticMember = 1;
	// LazySingleton.createString();
	// }
}
