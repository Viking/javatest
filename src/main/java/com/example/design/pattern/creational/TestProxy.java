package com.example.design.pattern.creational;

import java.lang.reflect.Proxy;

public class TestProxy {
	public static void main(String[] args) {
		BookFacadeImpl bookFacadeImpl = new BookFacadeImpl();
		BookFacadeProxy proxy = new BookFacadeProxy(bookFacadeImpl);
		BookFacade bookProxy = (BookFacade) Proxy.newProxyInstance(bookFacadeImpl.getClass().getClassLoader(),
				bookFacadeImpl.getClass().getInterfaces(), proxy);
		bookProxy.addBook();
	}
}
