package com.example.design.pattern.creational;

/**
 * 使用内部类来实现单例：（1）由于类加载是单线程的，故可以替代synchronized关键字解决并发问题。
 * （2）当单例类加载时，其内部类不会初始化。只当调用getInstance方法时，才会创建单例类
 * 
 * @author wangpeng
 *
 */
public class StaticSingleton {

	private StaticSingleton() {
		// TODO Auto-generated constructor stub
		System.out.println("StaticSingleton is create");
	}

	/**
	 * 持有单例对象的内部类
	 */
	private static class SingletonHolder {
		private static StaticSingleton instance = new StaticSingleton();
	}

	public static StaticSingleton getInstance() {
		return SingletonHolder.instance;
	}

	public static void createString() {
		System.out.println("createString in Singleton");
	}

	public static void main(String[] args) {
		StaticSingleton.getInstance();
	}
}
