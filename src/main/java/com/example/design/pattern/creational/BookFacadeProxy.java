package com.example.design.pattern.creational;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * JDK动态代理代理类
 * 
 * @author student
 * 
 */
public class BookFacadeProxy implements InvocationHandler {

	private Object target;

	public BookFacadeProxy(Object target) {
		// TODO Auto-generated constructor stub
		this.target = target;
	}

	@Override
	/**
	 * 调用方法
	 */
	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
		Object result = null;
		System.out.println("事务开始");
		// 执行方法
		result = method.invoke(target, args);
		System.out.println("事务结束");
		return result;
	}

}