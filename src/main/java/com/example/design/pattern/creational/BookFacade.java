package com.example.design.pattern.creational;

public interface BookFacade {
	public void addBook();
}