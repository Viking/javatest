package com.example.design.pattern.creational;

/**
 * 饿汉式单例：在使用该类的静态成员时，无论有没有使用单例类，都会创建单例对象
 * 
 * @author wangpeng
 *
 */
public class Singleton {

	private Singleton() {
		// TODO Auto-generated constructor stub
		System.out.println("Singleton is create");
	}

	public static int staticMember = 0;

	private static Singleton instance = new Singleton();

	public static Singleton getInstance() {
		return instance;
	}

	public static void createString() {
		System.out.println("createString in Singleton");
	}

	public static void main(String[] args) {
		// 输出Singleton is create和createString in Singleton
		Singleton.staticMember = 1;
		// Singleton.createString();
	}
}
