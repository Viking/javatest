package com.example.design.pattern.structural;

/**
 * 适配器模式-》目标角色：客户端期待得到的接口
 *
 * @see http://www.cnblogs.com/xrq730/p/4906487.html
 * Created by wangpeng on 25/01/2018.
 */
public interface ClassTarget {

    public void sampleOperation1();

    public void sampleOperation2();
}
