package com.example.design.pattern.structural;

/**
 * 适配器模式-》源角色：需要被适配的接口
 *
 * @see http://www.cnblogs.com/xrq730/p/4906487.html
 * Created by wangpeng on 25/01/2018.
 */
public class ClassAdaptee {

    public void sampleOperation1() {
    }
}
