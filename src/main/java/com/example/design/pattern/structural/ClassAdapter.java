package com.example.design.pattern.structural;

/**
 * 适配器模式——》核心：适配器把源接口转换成目标接口
 *
 * @see http://www.cnblogs.com/xrq730/p/4906487.html
 * Created by wangpeng on 25/01/2018.
 */
public class ClassAdapter extends ClassAdaptee implements ClassTarget {

    @Override
    public void sampleOperation2() {

    }
}
