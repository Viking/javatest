package com.example.push;

import org.apache.commons.httpclient.*;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class HttpUtils {

    private static final Logger LOGGER = LogManager.getLogger(HttpUtils.class);

	private HttpUtils() {
	}

	private static MultiThreadedHttpConnectionManager httpConnectionManager = new MultiThreadedHttpConnectionManager();
	private static HttpClient httpClient = new HttpClient(httpConnectionManager);

	static {

		// 每主机最大连接数和总共最大连接数，通过hosfConfiguration设置host来区分每个主机
		httpClient.getHttpConnectionManager().getParams().setDefaultMaxConnectionsPerHost(100);
		httpClient.getHttpConnectionManager().getParams().setMaxTotalConnections(1000);
		httpClient.getHttpConnectionManager().getParams().setConnectionTimeout(5000);
		httpClient.getHttpConnectionManager().getParams().setSoTimeout(5000);
		httpClient.getHttpConnectionManager().getParams().setTcpNoDelay(true);
		httpClient.getHttpConnectionManager().getParams().setLinger(1000);
		// 失败的情况下会进行3次尝试,成功之后不会再尝试
		httpClient.getHttpConnectionManager().getParams().setParameter(HttpMethodParams.RETRY_HANDLER, new DefaultHttpMethodRetryHandler());
	}

	/**
	 * GET方式请求
	 * 
	 * @param thirdURL
	 * @return
	 * @throws BusinessException
	 */
	public static String executeWithHttp(String thirdURL) throws BusinessException {
		HttpMethod httpMethod = new GetMethod(thirdURL);
		String result = null;
		URI uri = null;
		try {
			uri = httpMethod.getURI();
			final long startTime = System.currentTimeMillis();
			httpClient.getHttpConnectionManager().getParams().setSoTimeout(10000);
			int statusCode = httpClient.executeMethod(httpMethod);
			LOGGER.info("sendHttpClient executeTime[{}]毫秒:{}", (System.currentTimeMillis() - startTime), uri.toString());
			if (statusCode != HttpStatus.SC_OK) {
				LOGGER.error("执行HttpGet方法出错[{}]：", statusCode, uri.toString());
			}
			result = httpMethod.getResponseBodyAsString();
			if (LOGGER.isInfoEnabled()) {
				LOGGER.info("<<<<--{},URL={}", result, uri.toString());
			}
		} catch (Exception e) {
			LOGGER.error("访问[{}]发生异常：{}", uri != null ? uri.toString() : thirdURL, e);
			throw new BusinessException("HttpClient GET 1 error:" + e.getMessage(), e, true);
		} finally {
			httpMethod.releaseConnection();
		}
		return result;
	}

	/**
	 * GET方式请求
	 * 
	 * @param thirdURL
	 * @param params
	 * @return
	 * @throws BusinessException
	 */
	public static String executeWithHttp(String thirdURL, Map<String, String> params) throws BusinessException {
		HttpMethod httpMethod = new PostMethod(thirdURL);
		String result = null;
		URI uri = null;
		try {
			NameValuePair[] nvps = convertMapToNameValuePair(params);
			if (nvps != null && (nvps.length > 0)) {
				httpMethod.setQueryString(nvps);
			}
			uri = httpMethod.getURI();
			final long startTime = System.currentTimeMillis();
			int statusCode = httpClient.executeMethod(httpMethod);
			LOGGER.info("sendHttpClient executeTime[{}]毫秒:{}", (System.currentTimeMillis() - startTime), uri.toString());
			if (statusCode != HttpStatus.SC_OK) {
				LOGGER.error("执行HttpGet方法出错[{}]：", statusCode, uri.toString());
			}
			result = httpMethod.getResponseBodyAsString();
			if (LOGGER.isInfoEnabled()) {
				LOGGER.info("<<<<--{},URL={}", result, uri.toString());
			}
		} catch (Exception e) {
			LOGGER.error("访问[{}]发生异常：{}", uri != null ? uri.toString() : thirdURL, e);
			throw new BusinessException("HttpClient GET 2 error:" + e.getMessage(), e, true);
		} finally {
			httpMethod.releaseConnection();
		}
		return result;
	}

	private static NameValuePair[] convertMapToNameValuePair(Map<String, ? extends Object> params) {
		if (params == null) {
			return null;
		}
		Set<String> paramsSet = params.keySet();
		NameValuePair[] nvps = new NameValuePair[paramsSet.size()];
		int i = 0;
		for (Iterator<String> it = paramsSet.iterator(); it.hasNext(); i++) {
			String name = it.next();
			Object value = params.get(name);
			nvps[i] = new NameValuePair(name, value.toString());
		}
		return nvps;
	}

}
