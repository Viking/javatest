package com.example.push;

import java.util.Date;

/**
 * 
 * @author Sida.Qu
 * 
 * @date 2015-12-8
 * 
 */
public class Message {

	@SuppressWarnings("unused")
	private static final long serialVersionUID = -4251505144391404525L;

	private String sessionID;

	private String messageID;

	private long fromUserID;

	private long toUserID;

	private String brand;

	private int status;

	private Date createTime;

	private Date deleteTime;

	private Date updateTime;

	private Date readTime;

	private String platform;

	private String clientVersion;

	private String msg;

	private int type;

	private String pathID;

	private String ip;

	private int action;

	private String landingPage;

	private String pushType;

	private int hide;// 2垃圾信标识不给接收方发信

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public int getAction() {
		return action;
	}

	public void setAction(int action) {
		this.action = action;
	}

	public String getMessageID() {
		return messageID;
	}

	public void setMessageID(String messageID) {
		this.messageID = messageID;
	}

	public String getSessionID() {
		return sessionID;
	}

	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}

	public long getFromUserID() {
		return fromUserID;
	}

	public void setFromUserID(long fromUserID) {
		this.fromUserID = fromUserID;
	}

	public long getToUserID() {
		return toUserID;
	}

	public void setToUserID(long toUserID) {
		this.toUserID = toUserID;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public Date getUpdateDate() {
		return updateTime;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateTime = updateDate;
	}

	public Date getDeleteDate() {
		return deleteTime;
	}

	public void setDeleteDate(Date deleteDate) {
		this.deleteTime = deleteDate;
	}

	public Date getCreateDate() {
		return createTime;
	}

	public void setCreateDate(Date createDate) {
		this.createTime = createDate;
	}

	public Date getReadDate() {
		return readTime;
	}

	public void setReadDate(Date readDate) {
		this.readTime = readDate;
	}

	public String getPlatform() {
		return platform;
	}

	public void setPlatform(String platform) {
		this.platform = platform;
	}

	public String getClientVersion() {
		return clientVersion;
	}

	public void setClientVersion(String clientVersion) {
		this.clientVersion = clientVersion;
	}

	public String getContent() {
		return msg;
	}

	public void setContent(String content) {
		this.msg = content;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public String getPathID() {
		return pathID;
	}

	public void setPathID(String pathID) {
		this.pathID = pathID;
	}

	public String getPushType() {
		return pushType;
	}

	public void setPushType(String pushType) {
		this.pushType = pushType;
	}

	public String getLandingPage() {
		return landingPage;
	}

	public void setLandingPage(String landingPage) {
		this.landingPage = landingPage;
	}

	public int getHide() {
		return hide;
	}

	public void setHide(int hide) {
		this.hide = hide;
	}
}
