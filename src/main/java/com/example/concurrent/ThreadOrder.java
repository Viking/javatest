package com.example.concurrent;

/**
 * 指定线程执行顺序：通过synchronized共享对象锁加上volatile可见变量来实现
 * 
 * @author wangpeng
 *
 */
public class ThreadOrder {

	private volatile int orderNum = 1;

	public synchronized void methodA() {
		try {
			while (orderNum != 1) {
				wait();
			}
			for (int i = 0; i < 2; i++) {
				System.out.println("AAA");
			}
			orderNum = 2;
			notifyAll();
		} catch (InterruptedException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	public synchronized void methodB() {
		try {
			while (orderNum != 2) {
				wait();
			}
			for (int i = 0; i < 2; i++) {
				System.out.println("BBB");
			}
			orderNum = 3;
			notifyAll();
		} catch (InterruptedException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	public synchronized void methodC() {
		try {
			while (orderNum != 3) {
				wait();
			}
			for (int i = 0; i < 2; i++) {
				System.out.println("CCC");
			}
			orderNum = 1;
			notifyAll();
		} catch (InterruptedException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
}
