package com.geeksforgeeks.string;

public class StringExample {

	/**
	 * 字符串反转：不用递归，时间复杂度O(n)
	 * 
	 * @param str
	 */
	public static void reverse(String str) {
		int length = str.length();
		for (int i = length; i >= 1; i--) {
			// charAt从0开始去字符串对应位置的字符
			System.out.print(str.charAt(i - 1));
		}
	}

	/**
	 * 字符串反转：使用递归，时间复杂度O(n)
	 * 
	 * @param str
	 */
	public static void reverseWithRecursion(String str) {
		if (str == null || str.length() <= 1) {
			System.out.print(str);
		} else {
			// 获取字符串最后一个位置的字符
			System.out.print(str.charAt(str.length() - 1));
			// 从0开始截取到最后一个字符长度的字符串
			reverseWithRecursion(str.substring(0, str.length() - 1));
		}
	}

	/**
	 * 字符串的全排列，结果重复：使用递归，时间复杂度是 O(n*n!)
	 * 
	 * @param str
	 *            string to calculate permutation for
	 * @param l
	 *            starting index
	 * @param r
	 *            end index
	 */
	public static void permute(String str, int l, int r) {
		if (l == r) {
			System.out.println(str);
		} else {
			for (int i = l; i <= r; i++) {
				str = swap(str, l, i);// 将第一个字符与后面的字符交换
				permute(str, l + 1, r);// 对后面所有的字符进行全排列
				str = swap(str, l, i);// 再将之前交换的字符交换回来，以便第一个字符再与其他字符交换
			}
		}
	}

	/**
	 * Swap Characters at position
	 * 
	 * @param a
	 *            string value
	 * @param i
	 *            position 1
	 * @param j
	 *            position 2
	 * @return swapped string
	 */
	private static String swap(String a, int i, int j) {
		char temp;
		char[] charArray = a.toCharArray();
		temp = charArray[i];
		charArray[i] = charArray[j];
		charArray[j] = temp;
		return String.valueOf(charArray);
	}

    /**
     * Return maximum occurring character in an input string
     * Time Complexity: O(n)
     * Space Complexity: O(1) — Because we are using fixed space (Hash array) irrespective of input string size.
     *
     * @param str
     * @return
     * @see https://www.geeksforgeeks.org/return-maximum-occurring-character-in-the-input-string/
     */
    public static char getMaxOccuringChar(String str) {
        // Create array to keep the count of individual
        // characters and initialize the array as 0
        int count[] = new int[256];

        // Construct character count array from the input
        // string.
        int len = str.length();
        for (int i = 0; i < len; i++)
            count[str.charAt(i)]++;

        int max = -1;  // Initialize max count
        char result = ' ';   // Initialize result

        // Traversing through the string and maintaining
        // the count of each character
        for (int i = 0; i < len; i++) {
            // 如果max小于字符出现次数，则将max的值设置为该字符串出现的最大次数，并保存字符串的值到result变量中
            if (max < count[str.charAt(i)]) {
                max = count[str.charAt(i)];
                result = str.charAt(i);
            }
        }
        return result;
    }

    public static int kthNonRepeating(String str, int k) {
        return 0;
    }

}
