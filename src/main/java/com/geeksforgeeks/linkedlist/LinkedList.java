package com.geeksforgeeks.linkedlist;

public class LinkedList {

	Node head;

	/**
	 * 打印链表中间的元素 Traverse linked list using two pointers. Move one pointer by one and other pointer by
	 * two. When the fast pointer reaches end slow pointer will reach middle of the linked list.
	 * 
	 * @param head
	 * @see https://www.geeksforgeeks.org/write-a-c-function-to-print-the-middle-of-the-linked-list/
	 */
	public void printMiddle() {
		Node slow_ptr = head;
		Node fast_ptr = head;

		if (head != null) {
			while (fast_ptr != null && fast_ptr.next != null) {
				fast_ptr = fast_ptr.next.next;
				slow_ptr = slow_ptr.next;
			}
			System.out.println("The middle element is[" + slow_ptr.data + "]");
		}
	}

	/**
	 * Inserts a new Node at front of the list.
	 * 
	 * @param new_data
	 */
	public void push(int new_data) {
		/*
		 * 1 & 2: Allocate the Node & Put in the data
		 */
		Node new_node = new Node(new_data);

		/* 3. Make next of new Node as head */
		new_node.next = head;

		/* 4. Move the head to point to new Node */
		head = new_node;
	}

	/**
	 * This function prints contents of linked list starting from the given node
	 */
	public void printList() {
		Node tnode = head;
		while (tnode != null) {
			System.out.print(tnode.data + "->");
			tnode = tnode.next;
		}
		System.out.println("NULL");
	}
}
