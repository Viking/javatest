package com.geeksforgeeks.search;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * 二分查找法：用于排好序的数组
 * 
 * @author wangpeng
 * @see http://www.baeldung.com/java-binary-search
 * @see http://www.cnblogs.com/ider/archive/2012/04/01/binary_search.html
 */
public class BinarySearch {

	/**
	 * 循环二分查找，返回第一次出现该值的位置
	 * 
	 * @param sortedArray
	 *            已排序的数组
	 * @param key
	 *            需要找的值
	 * @param low
	 *            起始查找位置
	 * @param high
	 *            结束查找位置
	 * @return 值在数组中的位置，从0开始。找不到返回-1
	 */
	public int runBinarySearchIteratively(int[] sortedArray, int key, int low, int high) {
		while (low <= high) {
			int mid = (low + high) / 2;// 中间位置
			int midVal = sortedArray[mid];// 中值
			if (midVal < key) {// 大于中值时在中值后面找
				low = mid + 1;
			} else if (midVal > key) {// 小于中值时在中值前面找
				high = mid - 1;
			} else if (midVal == key) {// 等于中值
				return mid;
			}
		}
		// 返回-1，即查找失败
		return -1;
	}

	/**
	 * 递归二分查找
	 * 
	 * @param sortedArray
	 *            已排序的数组
	 * @param key
	 *            要查找的值
	 * @param low
	 *            起始查找位置
	 * @param high
	 *            结束查找位置
	 * @return 值在数组中的位置，从0开始。找不到返回-1
	 */
	public int runBinarySearchRecursively(int[] sortedArray, int key, int low, int high) {
		if (high < low) {
			return -1;
		}
		int mid = (low + high) / 2;// 中间位置
		int midVal = sortedArray[mid];// 中值
		if (key == midVal) {
			return mid;
		} else if (key < midVal) {// 小于中值时在中值前面找
			return runBinarySearchRecursively(sortedArray, key, low, mid - 1);
		} else {// 大于中值时在中值后面找
			return runBinarySearchRecursively(sortedArray, key, mid + 1, high);
		}
	}

	public int runBinarySearchUsingJavaArrays(int[] sortedArray, Integer key) {
		int index = Arrays.binarySearch(sortedArray, key);
		return index;
	}

	public int runBinarySearchUsingJavaCollections(List<Integer> sortedList, Integer key) {
		int index = Collections.binarySearch(sortedList, key);
		return index;
	}

}
