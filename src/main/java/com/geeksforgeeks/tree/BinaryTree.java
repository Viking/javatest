package com.geeksforgeeks.tree;

import java.util.*;

/**
 * 二叉树算法练习
 * 
 * @author wangpeng
 * @see http://www.lintcode.com/problem/?tag=binary-tree
 * @see https://www.geeksforgeeks.org/binary-tree-data-structure/
 */
public class BinaryTree {

	public TreeNode root;
	public static int preIndex = 0;

	/**
	 * 二叉树中序遍历（左根右）：使用递归
	 * 
	 * @param root
	 */
	public void inorder(TreeNode root) {
		if (root == null) {
			return;
		}

		inorder(root.left);
		System.out.print(root.val + " ");
		inorder(root.right);
	}

	/**
	 * 二叉树中序遍历（左根右）：不使用递归，使用栈，时间复杂度O(n)
	 * 
	 * @see https://www.geeksforgeeks.org/inorder-tree-traversal-without-recursion/
	 */
	public void inorderWithOutRecursion(TreeNode root) {
		if (root == null) {
			return;
		}

		// 将根节点和左子树节点全部压到栈中
		Stack<TreeNode> stack = new Stack<>();
		while (root != null) {
			stack.push(root);
			root = root.left;
		}

		// 取出所有左子树节点、根节点、右子树节点
		TreeNode node = root;
		while (stack.size() > 0) {
			node = stack.pop();
			System.out.print(node.val + " ");

			// 右子树节点不为空，则将节点压入栈顶
			if (node.right != null) {
				node = node.right;
				if (node != null) {
					stack.push(node);
				}
			}
		}
	}

	/**
	 * 二叉树后序遍历（左右根）：使用递归
	 * 
	 * @param root
	 */
	public void postOrder(TreeNode root) {
		if (root == null) {
			return;
		}
		postOrder(root.left);
		postOrder(root.right);
		System.out.print(root.val + " ");
	}

	/**
	 * 二叉树后序遍历（左右根）：不使用递归，使用两个栈
	 * 
	 * @param root
	 */
	public void postOrderWithOutRecursion(TreeNode root) {
		if (root == null) {
			return;
		}

		Stack<TreeNode> stack1 = new Stack<>();
		Stack<TreeNode> stack2 = new Stack<>();

		stack1.push(root);

		TreeNode node = null;
		while (stack1.size() > 0) {
			// Pop an item from s1 and push it to s2
			node = stack1.pop();
			stack2.push(node);

			// Push left and right children of
			// removed item to s1 这里考虑到第二个栈，所以用先左后右的形式
			if (node.left != null) {
				stack1.push(node.left);
			}
			if (node.right != null) {
				stack1.push(node.right);
			}
		}

		// Print all elements of second stack
		while (!stack2.isEmpty()) {
			TreeNode temp = stack2.pop();
			System.out.print(temp.val + " ");
		}
	}

	/**
	 * 二叉树前序遍历（根左右）：使用递归
	 * 
	 * @see https://www.geeksforgeeks.org/iterative-preorder-traversal/
	 * @param root
	 */
	public void preOrder(TreeNode root) {
		if (root == null) {
			return;
		}
		System.out.print(root.val + " ");
		preOrder(root.left);
		preOrder(root.right);
	}

	/**
	 * 二叉树后序遍历：不使用递归
	 * 
	 * @see https://www.geeksforgeeks.org/iterative-postorder-traversal/
	 * @param root
	 */
	public void preOrderWithOutRecursion(TreeNode root) {
		if (root == null) {
			return;
		}

		// Create an empty stack and push root to it
		Stack<TreeNode> stack = new Stack<>();
		stack.push(root);

		/*
		 * Pop all items one by one. Do following for every popped item a) print it b) push its right child
		 * c) push its left child Note that right child is pushed first so that left is processed first
		 */
		TreeNode node = null;
		while (stack.size() > 0) {
			// Pop the top item from stack and print it
			node = stack.pop();
			System.out.print(node.val + " ");

			// Push right and left children of the popped node to stack
			if (node.right != null) {
				stack.push(node.right);
			}
			if (node.left != null) {
				stack.push(node.left);
			}
		}
	}

	/**
	 * 二叉树的高度：使用递归，时间复杂度O(n)
	 * 
	 * @param root
	 *            二叉树的根节点
	 * @return 二叉树的高度
	 */
	public int height(TreeNode root) {
		if (root != null) {
			// 左子树与右子树的高度取最大值加上当前节点
			return Math.max(height(root.left), height(root.right)) + 1;
		}
		return 0;
	}

	/**
	 * 二叉树的高度：使用迭代方式，时间复杂度O(n)
	 * 
	 * @param root
	 * @return
	 * @see https://www.geeksforgeeks.org/iterative-method-to-find-height-of-binary-tree/
	 */
	public int heightWithIterative(TreeNode root) {
		if (root == null) {
			return 0;
		}

		// 创建空队列保存二叉树的每一层节点
		Queue<TreeNode> queue = new LinkedList<>();

		// 把root节点加入队列并初始化高度为0
		queue.add(root);
		int height = 0;

		while (queue.size() > 0) {
			// 获取当前层级的节点数量
			int nodeCount = queue.size();
			if (nodeCount == 0) {
				break;
			}

			// 高度加1
			height++;

			// 取出并移除当前层级的节点，并将下一层级的节点放入队列中
			while (nodeCount > 0) {
				TreeNode node = queue.remove();
				if (node.left != null) {
					queue.add(node.left);
				}
				if (node.right != null) {
					queue.add(node.right);
				}
				nodeCount--;
			}
		}
		return height;
	}

	/**
	 * 使用层次遍历法将二叉树序列化成一个字符串
	 * 
	 * @param node
	 * @return
	 * @see https://www.programcreek.com/2014/05/leetcode-serialize-and-deserialize-binary-tree-java/
	 */
	public String serialize(TreeNode root) {
		if (root == null) {
			return "";
		}

		StringBuilder sb = new StringBuilder();
		LinkedList<TreeNode> queue = new LinkedList<TreeNode>();
		queue.add(root);

		while (!queue.isEmpty()) {
			TreeNode t = queue.poll();
			if (t != null) {
				sb.append(String.valueOf(t.val) + ",");
				queue.add(t.left);
				queue.add(t.right);
			} else {// 节点为空的情况
				sb.append("#,");
			}
		}
		return sb.toString();
	}

	/**
	 * 反序列化字符串为一棵二叉树
	 * 
	 * @param data
	 * @return
	 * @see https://www.programcreek.com/2014/05/leetcode-serialize-and-deserialize-binary-tree-java/
	 */
	public TreeNode deserialize(String data) {
		if (data == null || data.length() == 0)
			return null;

		String[] arr = data.split(",");
		TreeNode root = new TreeNode(Integer.parseInt(arr[0]));

		LinkedList<TreeNode> queue = new LinkedList<TreeNode>();
		queue.add(root);

		int i = 1;
		// 循环遍历字符串中的节点信息，因为是按照层次遍历，所以直接按先左后右的顺序遍历，每取一次node，执行一次i++操作
		while (!queue.isEmpty()) {
			TreeNode t = queue.poll();

			if (t == null)
				continue;

			if (!arr[i].equals("#")) {
				t.left = new TreeNode(Integer.parseInt(arr[i]));
				queue.offer(t.left);
			} else {
				t.left = null;
				queue.offer(null);
			}
			i++;

			if (!arr[i].equals("#")) {
				t.right = new TreeNode(Integer.parseInt(arr[i]));
				queue.offer(t.right);
			} else {
				t.right = null;
				queue.offer(null);
			}
			i++;
		}
		return root;
	}

	/**
	 * Search Range in Binary Search Tree，print all keys that k1<=key<=k2 in ascending order .Time
	 * Complexity: O(n) where n is the total number of keys in tree.
	 * 
	 * @param root
	 *            root: The root of the binary search tree
	 * @param k1
	 *            An integer
	 * @param k2
	 *            An integer
	 * @see https://www.geeksforgeeks.org/print-bst-keys-in-the-given-range/
	 */
	public void searchRange(TreeNode root, int k1, int k2) {
		if (root == null) {
			return;
		}

		// If value of root’s key is greater than k1, then recursively call in left subtree.
		if (k1 < root.val) {
			searchRange(root.left, k1, k2);
		}

		// If value of root’s key is in range, then print the root’s key.
		if (k1 <= root.val && k2 >= root.val) {
			System.out.print(root.val + " ");
		}

		// If value of root’s key is smaller than k2, then recursively call in right subtree.
		if (k2 > root.val) {
			searchRange(root.right, k1, k2);
		}
	}

	/**
	 * Binary Tree Level Order Traversal，return the up-bottom level order traversal of its nodes'
	 * values. O(n) where n is number of nodes in the binary tree
	 * 
	 * @param root
	 *            A Tree
	 * @see https://www.geeksforgeeks.org/level-order-tree-traversal/
	 */
	public void levelOrder(TreeNode root) {
		if (root == null) {
			return;
		}

		LinkedList<TreeNode> queue = new LinkedList<>();
		queue.add(root);

		TreeNode node = null;
		while (!queue.isEmpty()) {
			node = queue.poll();
			System.out.print(node.val + " ");

			if (node.left != null) {
				queue.add(node.left);
			}
			if (node.right != null) {
				queue.add(node.right);
			}
		}
	}

	/**
	 * Binary Tree Level Order Traversal，return the bottom-up level order traversal of its nodes'
	 * values. O(n) where n is number of nodes in the binary tree
	 * 
	 * @param root
	 *            A Tree
	 * @see https://www.jiuzhang.com/solution/binary-tree-level-order-traversal-ii/
	 */
	public List<List<Integer>> levelOrderBottom(TreeNode root) {
		if (root == null) {
			return null;
		}

		LinkedList<TreeNode> queue = new LinkedList<>();
		queue.offer(root);

		List<List<Integer>> resultList = new ArrayList<>();

		while (!queue.isEmpty()) {
			int size = queue.size();
			List<Integer> level = new ArrayList<>();

			// 取出队列里的元素，放到level集合中
			for (int i = 0; i < size; i++) {
				TreeNode head = queue.poll();
				level.add(head.val);

				if (head.left != null) {
					queue.offer(head.left);
				}
				if (head.right != null) {
					queue.offer(head.right);
				}
			}
			resultList.add(level);
		}
		Collections.reverse(resultList);// 反转集合
		return resultList;
	}

	/**
	 * 验证满二叉树：如果一棵二叉树所有节点都有零个或两个子节点, 那么这棵树为满二叉树. 反过来说, 满二叉树中不存在只有一个子节点的节点.
	 * 
	 * @param root
	 *            the given tree
	 * @return Whether it is a full tree
	 */
	public boolean isFullTree(TreeNode root) {
		if (root == null) {
			return true;
		}

		if (root.left != null && root.right == null) {
			return false;
		}
		if (root.right != null && root.left == null) {
			return false;
		}

		boolean isLeft = isFullTree(root.left);
		boolean isRight = isFullTree(root.right);

		return isLeft && isRight;
	}

	/**
	 * 二叉树的最大节点 · Binary Tree Maximum Node
	 * 
	 * @param root
	 * @return
	 * @see https://www.geeksforgeeks.org/find-maximum-or-minimum-in-binary-tree/
	 */
	public TreeNode findMax(TreeNode root) {
		if (root == null) {
			return root;
		}

		TreeNode left = findMax(root.left);
		TreeNode right = findMax(root.right);
		return max(root, max(left, right));
	}

	TreeNode max(TreeNode a, TreeNode b) {
		if (a == null) {
			return b;
		}
		if (b == null) {
			return a;
		}
		if (a.val > b.val) {
			return a;
		}
		return b;
	}

	/**
	 * 二叉树的所有路径：Given a binary tree, print all root-to-leaf paths.Time Complexity: O(n2) where n is
	 * number of nodes.Time Complexity: O(n2) where n is number of nodes.
	 * 
	 * @param root
	 * @return
	 * @see https://www.jiuzhang.com/solution/binary-tree-paths/
	 * @see https://www.geeksforgeeks.org/given-a-binary-tree-print-all-root-to-leaf-paths/
	 */
	public List<String> binaryTreePaths(TreeNode root) {
		List<String> paths = new ArrayList<>();
		if (root == null) {
			return paths;
		}

		List<String> leftPaths = binaryTreePaths(root.left);
		List<String> rightPaths = binaryTreePaths(root.right);

		for (String path : leftPaths) {
			paths.add(root.val + "->" + path);
		}
		for (String path : rightPaths) {
			paths.add(root.val + "->" + path);
		}

		// root is a leaf
		if (paths.size() == 0) {
			paths.add("" + root.val);
		}
		return paths;
	}

	/**
	 * Given two trees, return true if they are structurally identical.Complexity of the identicalTree()
	 * will be according to the tree with lesser number of nodes. Let number of nodes in two trees be m
	 * and n then complexity of sameTree() is O(m) where m < n.
	 *
	 * @param a
	 * @param b
	 * @return
	 * @see https://www.geeksforgeeks.org/write-c-code-to-determine-if-two-trees-are-identical/
	 */
	public boolean identicalTrees(TreeNode a, TreeNode b) {
		if (a == null && b == null) {
			return true;
		}

		if (a != null && b != null) {
			return (a.val == b.val && identicalTrees(a.left, b.left) && identicalTrees(a.right, b.right));
		}
		return false;
	}

	/**
	 * 在root为根的二叉树中找A,B的公共祖先：使用单遍历法，时间复杂度O(n)
	 * 
	 * @param root
	 *            二叉树的根节点
	 * @param n1
	 *            节点1
	 * @param n2
	 *            节点2
	 * @return 公共祖先节点
	 * @see https://www.geeksforgeeks.org/lowest-common-ancestor-binary-tree-set-1/
	 * @see http://www.lintcode.com/zh-cn/problem/lowest-common-ancestor/#
	 * @see https://www.jianshu.com/p/118dfcb1d606
	 */
	public TreeNode findLCA(TreeNode root, int n1, int n2) {
		// 如果root为空，则返回空
		if (root == null) {
			return null;
		}

		// 如果root等于其中某个node，则返回root
		if (root.val == n1 || root.val == n2) {
			return root;
		}

		// 在左子树和右子树中查找n1、n2
		TreeNode left_lca = findLCA(root.left, n1, n2);
		TreeNode right_lca = findLCA(root.right, n1, n2);

		// 如果left和right都有结果返回，说明root是最小公共祖先
		if (left_lca != null && right_lca != null) {
			System.out.println("left_lca----" + left_lca.val + "----right_lca----" + right_lca.val);
			return root;
		}

		// 如果只有left有返回值，说明left的返回值是最小公共祖先；如果只有right有返回值，说明right的返回值是最小公共祖先;
		if (left_lca != null) {
			return left_lca;
		} else {
			return right_lca;
		}
	}

	/**
	 * Construct Tree from given Inorder and Preorder traversals
	 * 
	 * @param in
	 * @param pre
	 * @param inStrt
	 * @param inEnd
	 * @see https://www.geeksforgeeks.org/construct-tree-from-given-inorder-and-preorder-traversal/
	 * @return
	 */
	public TreeNode buildTree(char in[], char pre[], int inStrt, int inEnd) {
		if (inStrt > inEnd) {
			return null;
		}

		// 使用preIndex在前序遍历序列中获取当前的节点，并对preIndex自增
		TreeNode treeNode = new TreeNode(pre[preIndex++]);

		// 如果这个节点没有子节点则返回
		if (inStrt == inEnd) {
			return treeNode;
		} else {
			/* Else find the index of this node in Inorder traversal */
			int inIndex = search(in, inStrt, inEnd, treeNode.ch_val);
			/*
			 * Using index in Inorder traversal, construct left and right subtress
			 */
			treeNode.left = buildTree(in, pre, inStrt, inIndex - 1);
			treeNode.right = buildTree(in, pre, inIndex + 1, inEnd);
		}
		return treeNode;

	}

	private int search(char arr[], int strt, int end, char value) {
		int i;
		for (i = strt; i <= end; i++) {
			if (arr[i] == value) {
				return i;
			}
		}
		return i;
	}

	public void printInorder(TreeNode node) {
		if (node == null)
			return;

		/* first recur on left child */
		printInorder(node.left);

		/* then print the data of node */
		System.out.print(node.ch_val + " ");

		/* now recur on right child */
		printInorder(node.right);
	}
}
