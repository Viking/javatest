package com.geeksforgeeks.tree;

import java.util.ArrayList;
import java.util.List;

public class BinarySearchTree {

	/**
	 * 验证二叉搜索树：使用中序遍历法，Time Complexity: O(n)
	 * 
	 * @param root
	 * @return
	 * @see http://blog.csdn.net/ljiabin/article/details/41699241
	 * @see http://www.lintcode.com/zh-cn/problem/validate-binary-search-tree/
	 */
	public boolean isValidBST(TreeNode root) {
		List<Integer> list = new ArrayList<>();
		inorder(root, list);
		for (int i = 0; i < list.size() - 1; i++) {
			if (list.get(i) >= list.get(i + 1)) {
				return false;
			}
		}
		return true;
	}

	private void inorder(TreeNode node, List<Integer> list) {
		if (node == null) {
			return;
		}
		inorder(node.left, list);
		list.add(node.val);
		inorder(node.right, list);
	}
}
