package com.geeksforgeeks.tree;

public class TreeNode {

	int val;
	char ch_val;
	TreeNode left, right;

	public TreeNode(int item) {
		val = item;
		left = right = null;
	}

	public TreeNode(char ch_val) {
		// TODO Auto-generated constructor stub
		this.ch_val = ch_val;
		this.left = this.right = null;
	}

	@Override
	public String toString() {
		return "TreeNode [val=" + val + ", ch_val=" + ch_val + ", left=" + left + ", right=" + right + "]";
	}

}
