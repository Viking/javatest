package com.example.concurrent;

import java.util.Date;
import java.util.concurrent.CyclicBarrier;

/**
 * CyclicBarrier从字面理解是指循环屏障，它可以协同多个线程，让多个线程在这个屏障前等待，直到所有线程都达到了这个屏障时，再一起继续执行后面的动作。
 * 
 * @author wangpeng
 * @see https://www.cnblogs.com/dolphin0520/p/3920397.html
 *
 */
public class CyclicBarrierThreadTest extends Thread {

	private CyclicBarrier cyclicBarrier;
	private int sleepSecond;

	public CyclicBarrierThreadTest(CyclicBarrier cyclicBarrier, int sleepSecond) {
		// TODO Auto-generated constructor stub
		this.cyclicBarrier = cyclicBarrier;
		this.sleepSecond = sleepSecond;
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		try {
			System.out.println(this.getName() + "运行了");
			Thread.sleep(sleepSecond * 1000);
			System.out.println(this.getName() + "准备等待了, 时间为" + new Date());
			cyclicBarrier.await();
			System.out.println(this.getName() + "结束等待了, 时间为" + new Date());
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		Runnable runnable = new Runnable() {
			@Override
			public void run() {
				System.out.println("CyclicBarrier的所有线程await()结束了，我运行了, 时间为" + new Date());
			}
		};

		CyclicBarrier cb = new CyclicBarrier(3, runnable);
		CyclicBarrierThreadTest cbt0 = new CyclicBarrierThreadTest(cb, 3);
		CyclicBarrierThreadTest cbt1 = new CyclicBarrierThreadTest(cb, 6);
		CyclicBarrierThreadTest cbt2 = new CyclicBarrierThreadTest(cb, 9);
		cbt0.start();
		cbt1.start();
		cbt2.start();
	}
}
