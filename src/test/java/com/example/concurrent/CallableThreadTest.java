package com.example.concurrent;

import java.util.Date;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;

/**
 * 测试 Callable、Future和FutureTask
 * 
 * @see http://www.cnblogs.com/xrq730/p/4872722.html
 * @author wangpeng
 *
 */
public class CallableThreadTest implements Callable<String> {

	@Override
	public String call() throws Exception {
		// TODO Auto-generated method stub
		System.out.println("进入CallableThread的call()方法, 开始睡觉, 睡觉时间为" + new Date());
		Thread.sleep(10000);
		return "123";
	}

	public static void main(String[] args) throws InterruptedException, ExecutionException {
		// ExecutorService es = Executors.newCachedThreadPool();
		// CallableThreadTest ct = new CallableThreadTest();
		// FutureTask<String> f = new FutureTask<String>(ct);
		// es.submit(ct);
		// es.shutdown();
		//
		// Thread.sleep(5000);
		// System.out.println("主线程等待5秒, 当前时间为" + new Date());
		//
		// String str = f.get();
		// System.out.println("Future已拿到数据, str = " + str + ", 当前时间为" + new Date());

		ExecutorService es = Executors.newCachedThreadPool();
		CallableThreadTest ct = new CallableThreadTest();
		FutureTask<String> f = new FutureTask<String>(ct);
		es.submit(f);
		es.shutdown();

		Thread.sleep(5000);
		System.out.println("主线程等待5秒, 当前时间为" + new Date());

		String str = f.get();
		System.out.println("Future已拿到数据, str = " + str + ", 当前时间为" + new Date());
	}
}
