package com.example.concurrent;

import java.util.concurrent.CountDownLatch;

/**
 * CountDownLatch主要提供的机制是当多个（具体数量等于初始化CountDownLatch时count参数的值）线程都达到了预期状态或完成预期工作时触发事件，
 * 
 * 其他线程可以等待这个事件来触发自己的后续工作。值得注意的是，CountDownLatch是可以唤醒多个等待的线程的。
 * 
 * @author wangpeng
 * @see http://www.cnblogs.com/xrq730/p/4869671.html
 * @see https://www.cnblogs.com/dolphin0520/p/3920397.html
 *
 */
public class CountDownLatchTest {

	public static void main(String[] args) {
		// 初始化倒计时次数为2个
		CountDownLatch latch = new CountDownLatch(2);

		new Thread(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				try {
					System.out.println("子线程" + Thread.currentThread().getName() + "正在执行");
					Thread.sleep(3000);
					latch.countDown();// 计数器减1
					System.out.println("子线程" + Thread.currentThread().getName() + "执行完毕");
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}).start();

		new Thread(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				try {
					System.out.println("子线程" + Thread.currentThread().getName() + "正在执行");
					Thread.sleep(3000);
					latch.countDown();// 计数器减1
					System.out.println("子线程" + Thread.currentThread().getName() + "执行完毕");
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}).start();

		try {
			System.out.println("等待2个子线程执行完毕...");
			latch.await();// 让当前线程等待，直到计数器完成
			System.out.println("2个子线程已经执行完毕");
			System.out.println("继续执行主线程");
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}