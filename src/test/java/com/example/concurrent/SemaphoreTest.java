package com.example.concurrent;

import java.util.Date;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;

import org.junit.Test;

/**
 * 
 * Semaphore是非常有用的一个组件，它相当于是一个并发控制器，是用于管理信号量的。 构造的时候传入可供管理的信号量的数值，这个数值就是控制并发数量的，
 * 
 * 我们需要控制并发的代码，执行前先通过acquire方法获取信号，执行后通过release归还信号 。
 * 
 * 每次acquire返回成功后，Semaphore可用的信号量就会减少一个，如果没有可用的信号，acquire调用就会阻塞，
 * 
 * 等待有release调用释放信号后，acquire才会得到信号并返回。
 * 
 * 
 * @author wangpeng
 *
 */
public class SemaphoreTest {

	@Test
	public void testSemaphore1() throws Exception {
		final Semaphore semaphore = new Semaphore(5);
		Runnable runnable = new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				try {
					semaphore.acquire();
					System.out.println(Thread.currentThread().getName() + "获得了信号量,时间为" + new Date());
					Thread.sleep(10000);
					System.out.println(Thread.currentThread().getName() + "释放了信号量,时间为" + new Date());
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				} finally {
					semaphore.release();
				}
			}
		};

		Thread[] threads = new Thread[10];
		for (int i = 0; i < threads.length; i++) {
			threads[i] = new Thread(runnable);
		}
		for (int i = 0; i < threads.length; i++) {
			threads[i].start();
		}
	}

	@Test
	public void testSemaphore2() throws Exception {
		final Semaphore semaphore = new Semaphore(5);
		Runnable runnable = new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				try {
					semaphore.acquire();
					System.out.println(Thread.currentThread().getName() + "获得了信号量,时间为" + new Date());
					Thread.sleep(10000);
					System.out.println(Thread.currentThread().getName() + "释放了信号量,时间为" + new Date());
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				} finally {
					semaphore.release();
				}
			}
		};

		ExecutorService executorService = Executors.newFixedThreadPool(20);
		for (int i = 0; i < 20; i++) {
			executorService.submit(runnable);
		}
		while (true) {

		}
	}
}
