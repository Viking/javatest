package com.example.concurrent;

import org.junit.Test;

public class ThreadOrderTest {

	/**
	 * 指定线程执行顺序：通过共享对象锁加上可见变量来实现
	 * 
	 * @throws Exception
	 */
	@Test
	public void test1() throws Exception {
		ThreadOrder threadOrder = new ThreadOrder();

		Thread thread1 = new Thread(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				threadOrder.methodA();
			}
		});

		Thread thread2 = new Thread(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				threadOrder.methodB();
			}
		});

		Thread thread3 = new Thread(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				threadOrder.methodC();
			}
		});

		thread1.start();
		thread2.start();
		thread3.start();
	}

	/**
	 * 通过主线程join()
	 * 
	 * @throws Exception
	 */
	@Test
	public void test2() throws Exception {
		Thread thread1 = new Thread(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				System.out.println("AAA");
			}
		});

		Thread thread2 = new Thread(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				System.out.println("BBB");
			}
		});

		Thread thread3 = new Thread(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				System.out.println("CCC");
			}
		});

		thread1.start();
		thread1.join();
		thread2.start();
		thread2.join();
		thread3.start();
		thread3.join();
	}
}
