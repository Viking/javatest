package com.example.concurrent;

import java.util.Date;
import java.util.concurrent.Exchanger;

/**
 * Exchanger用于在两个线程之间进行数据交换，注意也只能在两个线程之间进行数据交换。
 * 
 * 线程会阻塞在Exchanger的exchange方法上，直到另外一个线程也到了同一个Exchanger的exchange方法时，
 * 
 * 二者进行数据交换，然后两个线程继续执行自身相关的代码
 * 
 * @author wangpeng
 *
 */
public class ExchangerThreadTest extends Thread {

	private String str;
	private Exchanger<String> exchanger;
	private int sleepSecond;

	public ExchangerThreadTest(String str, Exchanger<String> exchanger, int sleepSecond) {
		// TODO Auto-generated constructor stub
		this.str = str;
		this.exchanger = exchanger;
		this.sleepSecond = sleepSecond;
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		try {
			System.out.println(this.getName() + "启动, 原数据为" + str + ", 时间为" + new Date());
			Thread.sleep(sleepSecond * 1000);
			str = exchanger.exchange(str);
			System.out.println(this.getName() + "交换了数据, 交换后的数据为" + str + ", 时间为" + new Date());
		} catch (InterruptedException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		Exchanger<String> exchanger = new Exchanger<>();
		ExchangerThreadTest et1 = new ExchangerThreadTest("111", exchanger, 5);
		ExchangerThreadTest et2 = new ExchangerThreadTest("222", exchanger, 5);
		ExchangerThreadTest et3 = new ExchangerThreadTest("333", exchanger, 5);
		ExchangerThreadTest et4 = new ExchangerThreadTest("444", exchanger, 5);
		et1.start();
		et2.start();
		et3.start();
		et4.start();
	}
}
