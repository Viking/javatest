package com.example.concurrent;

import java.util.concurrent.locks.ReentrantLock;

import org.junit.Test;

/**
 * 测试Thread类中的实例方法和静态方法
 * 
 * @author wangpeng
 *
 */
public class ThreadTest {

	/**
	 * 测试interrupt方法：在线程受到阻塞时抛出一个中断信号，这样线程就得以退出阻塞状态。 没有被阻塞的线程，调用interrupt()方法是不起作用的
	 */
	@Test
	public void testInterrupt() {
		Thread thread = new Thread(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				for (int i = 0; i < 500000; i++) {
					System.out.println("i = " + (i + 1));
				}
			}
		});

		thread.start();

		thread.interrupt();
	}

	/**
	 * join：等待线程销毁
	 * 
	 * @throws Exception
	 */
	@Test
	public void testJoin() throws Exception {
		Thread thread = new Thread(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				int secondValue = (int) (Math.random() * 10000);
				System.out.println(secondValue);
				try {
					Thread.sleep(secondValue);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});

		thread.start();
		thread.join();
		System.out.println("我想当mt对象执行完毕之后我再执行，我做到了");
	}

	@Test
	public void test() throws Exception {
		ThreadGroup group = new ThreadGroup("ss");
		ReentrantLock lock;
	}
}
