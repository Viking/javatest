package com.example.push;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class PushTest {

    private static final Logger LOGGER = LogManager.getLogger(PushTest.class);
	public static final String pushCenterUrl = "http://pushcenter.baihe.com:18080/inner/push/send.json";
	public static final String channel = "apns";
	private static final ExecutorService EXECUTOR_SERVICE = Executors.newFixedThreadPool(500);

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("please input file name：");
		String fileName = sc.nextLine();
		final String msString = "明年的七夕再也不要一个人过，注册只需30秒，来百合遇见爱！";
		try {
            String json = FileUtils.readFileToString(new File(fileName), "utf-8");
			JSONArray array = JSONArray.parseArray(json);
			JSONObject object = null;
			for (int i = 0; i < array.size(); i++) {
				object = JSONObject.parseObject(array.getString(i));
				final String appID = object.getString("appID");
				final String apnsDeviceToken = object.getString("apnsDeviceToken");
				if (StringUtils.isNotBlank(appID) && StringUtils.isNotBlank(apnsDeviceToken)) {
					EXECUTOR_SERVICE.execute(new Runnable() {

						@Override
						public void run() {
							// TODO Auto-generated method stub
							JSONObject content = new JSONObject();
							content.put("title", "找个合适的人一起过七夕");
							content.put("msg", msString);
							content.put("alterText", msString);
							content.put("landingPageType", 112);
							content.put("pushtype", 010313);
							content.put("apnsToken", apnsDeviceToken);
							content.put("appID", appID);
							sendPush(content, "", channel, 1);
						}
					});
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * 测试normal push
	 */
	@Test
	public void testSendNormalPush() {
		try {
			String appID = "baihe";
			String apnsDeviceToken = "";
			String toUserID = "126032504";
			toUserID = "93910298";
			String msString = "赵丽颖说——明年的七夕再也不要一个人过，注册只需30秒，来百合遇见爱！";
			// TODO Auto-generated method stub
			JSONObject content = new JSONObject();
			content.put("title", "Normal Push测试");
			content.put("msg", msString);
			content.put("alterText", msString);
			content.put("landingPageType", 112);
			content.put("pushtype", 010313);
			content.put("apnsToken", apnsDeviceToken);
			content.put("appID", appID);
			sendPush(content, toUserID, "BaiXin", 1);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	/**
	 * 测试IM push
	 */
	@Test
	public void testSendIMPush() {
		try {
			Message message = new Message();
			message.setBrand("DZB");
			message.setClientVersion("0");
			message.setContent("你关注的[profilePage uid=114383705]dawei[/profilePage]上线了，特别提醒你，赶快去看看！");
			message.setFromUserID(124220316);
			message.setIp("172.16.37.18");
			message.setLandingPage("{\"android\":{\"type\":\"101\"},\"ios\":{\"type\":\"101\"}}");

			message.setPathID("06.00.60501");
			message.setPlatform("0");
			message.setToUserID(126032504);
			message.setType(1);

			sendImPush(message, message.getFromUserID() + "", message.getToUserID() + "");
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	/**
	 * 发送push
	 * 
	 * @param toUserID
	 *            接收人id
	 * @param channel
	 *            标识下发通道：BaiXin为百信，GeiTui为个推
	 */
	public static void sendPush(JSONObject content, String toUserID, String channel, int messageCategory) {
		// 业务参数
		JSONObject pushContent = new JSONObject();
		JSONObject message = new JSONObject();
		pushContent.put("title", content.getString("title"));
		pushContent.put("msg", content.getString("msg"));
		pushContent.put("receiver", toUserID);
		pushContent.put("type", content.getIntValue("landingPageType")); // type值决定跳转位置
		pushContent.put("landingPageType", content.getIntValue("landingPageType")); // landingPageType值决定跳转位置,6.4新增字段
		String landingPageType = content.getString("landingPageType");
		if ("53".equals(landingPageType)) {
			pushContent.put("url", content.getString("url"));
		}
		pushContent.put("aps", content.getJSONObject("aps"));// 直播push显示标题
		pushContent.put("lInfo", content.getJSONObject("lInfo"));// 直播push房间信息

		String appID = "74e39cd6a76b534ae896c5622dc389a0";
		message.put("appID", appID); // APPID
		message.put("pushtype", content.getString("pushtype")); // 用于统计打点
		message.put("msgBody", pushContent); // 消息体
		message.put("receiver", toUserID); // 此处必传，与pushContent中不重复
		message.put("messageCategory", messageCategory);

		JSONObject apnsMessage = new JSONObject();
		String alterText = content.getString("alterText");
		Map<String, Object> map = new HashMap<String, Object>(8);
		map.put("nt", content.getString("landingPageType"));// 跳转的位置，跟客户端协定好的值
		map.put("lpt", content.getString("landingPageType"));
		map.put("pt", content.getString("pushtype"));
		if ("53".equals(landingPageType)) {
			map.put("nu", content.getString("url")); // 活动push跳转的url
		} else {
			map.put("nu", toUserID);
		}
		map.put("t", "normal222"); // push的类型：im，normal
		map.put("r", toUserID);
		map.put("aps", content.getJSONObject("aps"));// 直播push显示标题
		map.put("lInfo", content.getJSONObject("lInfo"));// 直播push房间信息
		apnsMessage.put("alertBody", alterText);
		apnsMessage.put("appID", content.get("appID"));
		apnsMessage.put("apnsToken", content.get("apnsToken"));
		apnsMessage.put("customFields", map);

		JSONObject params = new JSONObject();
		params.put("message", message.toJSONString());
		params.put("apnsMessage", apnsMessage.toJSONString());
		params.put("userID", toUserID);

		// 调用pushCenter接口发送push
		StringBuilder sb = new StringBuilder();
		sb.append("traceID=").append("1");
		sb.append("&systemID=").append("1");
		sb.append("&serviceName=").append("sendPushCenter");
		params.put("channelKey", channel);
		try {
			sb.append("&params=").append(URLEncoder.encode(params.toJSONString(), "utf-8"));
			String result = HttpUtils.executeWithHttp(pushCenterUrl + "?" + sb.toString());
			if (LOGGER.isInfoEnabled()) {
				LOGGER.info("send pushCenter push params:{},result:{}", params.toJSONString(), result);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			if (LOGGER.isErrorEnabled()) {
				LOGGER.error("send pushCenter push params:{},error:{}", params.toJSONString(), e);
			}
		}
	}

	public void sendImPush(Message message, String sender, String receiver) {
		// TODO Auto-generated method stub
		String content = message.getContent();
		int imType = message.getType();
		long fromUserId = message.getFromUserID();
		long toUserId = message.getToUserID();

		// 公共参数
		StringBuffer data = new StringBuffer();
		data.append("traceID=").append("1");
		data.append("&systemID=").append("1");
		data.append("&serviceName=").append("sendMessage");

		// 业务参数
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("sender", fromUserId);
		jsonObject.put("receiver", toUserId);
		jsonObject.put("msgId", message.getMessageID());
		jsonObject.put("imType", imType);

		// 统计相关
		String pushType = "2010201";

		if (imType == 2) {// 语音消息
			JSONObject voiceJson = JSONObject.parseObject(content);
			jsonObject.put("voiceid", voiceJson.getString("voiceid"));
			jsonObject.put("duration", voiceJson.getString("duration"));
		} else if (imType == 6) {// 礼物信
			JSONObject giftJson = JSONObject.parseObject(content);
			jsonObject.put("giftInfo", giftJson);
		} else if (imType == 8) {
			JSONObject extend = new JSONObject();
			extend.put("imUpdateTipText", "由于您当前的版本较低，收到的下班见消息消息无法显示。请您升级到最新版本后查看。http://3g.baihe.com/html.php?action=download");
			jsonObject.put("extends", extend);
			jsonObject.put("text", content);
		} else if (imType == 10) {
			JSONObject extend = new JSONObject();
			extend.put("imUpdateTipText", "我赞了您的照片。");
			jsonObject.put("extends", extend);
			jsonObject.put("text", content);
		} else if (imType == 20) {
			JSONObject redBagJson = JSONObject.parseObject(content);
			String type = "";
			if (redBagJson.containsKey("type")) {
				type = redBagJson.getString("type");
			}
			if ("1".equals(type)) {
				jsonObject.put("text", "我是一个现金红包，点我领取");
			} else if ("2".equals(type)) {
				jsonObject.put("text", redBagJson.getString("describe"));
			}

		} else {// 文本消息
			jsonObject.put("text", content);
		}
		jsonObject.put("pathID", message.getPathID());
		jsonObject.put("type", imType);

		String pathid = message.getPathID();
		boolean isReply = false; // 标识是否是回复消息
		if (pathid != null) {
			String[] path = pathid.split("\\.");
			if (path.length == 3) {
				if (path[0].equals("05")) { // 回复引发的回复
					jsonObject.put("isReply", 1);
					isReply = true;
					pushType = "2010202";
					String source = path[1];
					if (source.equals("02") || source.equals("03")) {// 招呼引发的回复、批量引发的回复
						jsonObject.put("imFlag", 10);
					}
				}
			}
		}

		// 获取用户资料，如果未获取到用户资料，记录日志并返回
		JSONObject userInfo = new JSONObject();
		userInfo.put("uid", sender);
		userInfo.put("nickname", "dx");
		userInfo.put("iconurl", "http://photo.baihe.com/2015/12/30/120_150/2A8FBA724C76A376E6DCB4530017D313.jpg");
		userInfo.put("age", "22");
		userInfo.put("education", "本科");
		userInfo.put("income", "10000-15000");
		userInfo.put("longitude", "113.692602");
		userInfo.put("latitude", "34.798279");
		userInfo.put("isRealname", "0");
		userInfo.put("sex", "0");
		userInfo.put("citycode", "863301");
		userInfo.put("sourceId", sender);
		userInfo.put("destId", receiver);
		userInfo.put("height", "166");
		userInfo.put("marital", "未婚");
		jsonObject.put("userInfo", userInfo); // 安卓解析的是json对象，不能传字符串

		// // 公众号消息直接展示在push上
		// jsonObject.put("showContent", 0);

		// 落地页相关
		int landingPageTypeAndroid = 2;
		String urlAndroid = null; // 跳转页面
		int landingPageTypeIOS = 2;
		String urlIOS = null; // 跳转页面
		String anotherUserIDAndroid = null; // android 对方用户 id
		String anotherUserIDIOS = null;// ios 对方用户 id
		try {
			JSONObject landingPageJson = JSON.parseObject(message.getLandingPage());
			if (landingPageJson != null) {
				// 获取安卓push信息
				JSONObject androidLandingPage = landingPageJson.getJSONObject("android");
				if (androidLandingPage != null) {
					landingPageTypeAndroid = androidLandingPage.getInteger("type");
					urlAndroid = androidLandingPage.getString("url");
					anotherUserIDAndroid = androidLandingPage.getString("anotherUserID");
				}
				// 获取IOS push信息
				JSONObject iosLandingPage = landingPageJson.getJSONObject("ios");
				if (iosLandingPage != null) {
					landingPageTypeIOS = iosLandingPage.getInteger("type");
					urlIOS = iosLandingPage.getString("url");
					anotherUserIDIOS = iosLandingPage.getString("anotherUserID");
				}
			}
		} catch (Exception e) {
			LOGGER.error("sendMessage error message,fromUserId:[{}],toUserId:[{}],ERROR[{}]", fromUserId, toUserId, e);
		}
		if (urlAndroid != null && !urlAndroid.trim().isEmpty()) {
			jsonObject.put("url", urlAndroid);
		}
		if (message.getPushType() != null && !message.getPushType().trim().isEmpty()) {
			pushType = message.getPushType();
		}
		jsonObject.put("landingPageType", landingPageTypeAndroid);
		jsonObject.put("pushtype", pushType); // 沿用原字段名称，兼容客户端字段
		if (StringUtils.isNotBlank(anotherUserIDAndroid)) {
			jsonObject.put("oppuid", String.valueOf(anotherUserIDAndroid));
		}
		JSONObject msgJson = new JSONObject();
		msgJson.put("imType", imType);
		msgJson.put("sender", fromUserId); // 发送方
		msgJson.put("receiver", toUserId); // 接收方
		msgJson.put("mid", message.getMessageID()); // 消息ID
		msgJson.put("msgBody", jsonObject); // 消息体
		// 当前发送IM
		msgJson.put("messageCategory", 0);

		JSONObject apnsMessage = new JSONObject();
		// IM PUSH
		String alterText = null;
		String nickname = "赵丽颖";
		if (imType == 6) {// 如果是礼物信，设置礼物信 push 内容
			alterText = nickname + "给您送礼物了！请点击查看！";
		} else if (imType == 10) {// 照片点赞消息
			alterText = nickname + "给你赞了你的照片，快去看看吧！";
		} else {
			if (isReply) { // 如果是回复消息则设置回复 push，否则，为普通 push
				alterText = nickname + "给你【回复消息】啦！立即查看！";
			} else {
				alterText = "收到\"" + nickname + "\"新消息。";
			}
		}

		// 公众号逻辑
		try {
			if (jsonObject.getIntValue("showContent") == 1) {
				alterText = jsonObject.getString("text");

			}
		} catch (Exception e) {
			// 忽略
			LOGGER.error("sendMessage-getPublicUser error,fromUserId:[{}],toUserId:[{}],ERROR[{}]", fromUserId, toUserId, e);
		}
		Map<String, Object> map = new ConcurrentHashMap<String, Object>(8);
		map.put("nt", "21");// 跳转的位置，跟客户端协定好的值
		map.put("lpt", String.valueOf(landingPageTypeIOS));
		if (urlIOS != null && !urlIOS.trim().isEmpty()) {
			map.put("nu", urlIOS); // apns跳转url用字段nu标示
		}
		map.put("pt", pushType);
		map.put("t", "im"); // push的类型：im，normal
		map.put("s", String.valueOf(fromUserId)); // 发送者
		if (StringUtils.isNotBlank(anotherUserIDIOS)) {
			map.put("r", anotherUserIDIOS);
			map.put("nu", anotherUserIDIOS); // 兼容客户端
		}
		apnsMessage.put("alertBody", alterText);
		apnsMessage.put("customFields", map);

		JSONObject params = new JSONObject();
		params.put("message", msgJson.toJSONString());
		params.put("apnsMessage", apnsMessage.toJSONString());
		params.put("userID", toUserId);
		try {
			data.append("&params=").append(URLEncoder.encode(params.toJSONString(), "utf-8"));
			String result = HttpUtils.executeWithHttp(pushCenterUrl + "?" + data.toString());
			LOGGER.info("send push result:[{}],fromUserId:[{}],toUserId:[{}]", result, fromUserId, toUserId);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			LOGGER.error("sendMessage error message,fromUserId:[{}],toUserId:[{}],ERROR[{}]", fromUserId, toUserId, e);
		}
	}
}
