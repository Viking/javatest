package com.geeksforgeeks.tree;

import org.apache.logging.log4j.LogManager;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

public class BinaryTreeTest {

    private static final org.apache.logging.log4j.Logger LOGGER = LogManager.getLogger(BinaryTreeTest.class);

	/**
	 * 测试使用递归进行前序、中序、后序遍历二叉树
	 * 
	 * @throws Exception
	 */
	@Test
	public void testTraversal() throws Exception {
		BinaryTree tree = new BinaryTree();
		tree.root = new TreeNode(1);
		tree.root.left = new TreeNode(2);
		tree.root.right = new TreeNode(3);
		tree.root.left.left = new TreeNode(4);
		tree.root.left.right = new TreeNode(5);

		System.out.println("Preorder traversal of binary tree is ");
		tree.preOrder(tree.root);

		System.out.println("\nInorder traversal of binary tree is ");
		tree.inorder(tree.root);

		System.out.println("\nPostorder traversal of binary tree is ");
		tree.postOrder(tree.root);
	}

	/**
	 * 测试不使用递归进行前序、中序、后序遍历二叉树
	 * 
	 * @throws Exception
	 */
	@Test
	public void testTraversalWithOutRecursion() throws Exception {
		BinaryTree tree = new BinaryTree();
		tree.root = new TreeNode(1);
		tree.root.left = new TreeNode(2);
		tree.root.right = new TreeNode(3);
		tree.root.left.left = new TreeNode(4);
		tree.root.left.right = new TreeNode(5);

		System.out.println("Preorder traversal of binary tree is ");
		tree.preOrderWithOutRecursion(tree.root);

		System.out.println("\nInorder traversal of binary tree is ");
		tree.inorderWithOutRecursion(tree.root);

		System.out.println("\nPostorder traversal of binary tree is ");
		tree.postOrderWithOutRecursion(tree.root);
	}

	/**
	 * 测试使用层次遍历法将二叉树序列化成一个字符串
	 * 
	 * @throws Exception
	 */
	@Test
	public void testSerializeAndDeserialize() throws Exception {
		BinaryTree tree = new BinaryTree();
		tree.root = new TreeNode(1);
		tree.root.left = new TreeNode(2);
		tree.root.right = new TreeNode(3);
		tree.root.left.left = new TreeNode(4);
		tree.root.left.right = new TreeNode(5);

		String data = tree.serialize(tree.root);
		System.out.println(data);
		System.out.println(tree.deserialize(data));
	}

	/**
	 * Test search Range in Binary Search Tree
	 * 
	 * @throws Exception
	 */
	@Test
	public void testSearchRange() throws Exception {
		BinaryTree tree = new BinaryTree();
		tree.root = new TreeNode(20);
		tree.root.left = new TreeNode(8);
		tree.root.right = new TreeNode(22);
		tree.root.left.left = new TreeNode(4);
		tree.root.left.right = new TreeNode(12);

		int k1 = 10, k2 = 25;
		tree.searchRange(tree.root, k1, k2);
	}

	/**
	 * 测试二叉树的层级遍历
	 * 
	 * @throws Exception
	 */
	@Test
	public void testLevelOrder() throws Exception {
		BinaryTree tree = new BinaryTree();
		tree.root = new TreeNode(3);
		tree.root.left = new TreeNode(9);
		tree.root.right = new TreeNode(20);
		tree.root.left.left = new TreeNode(15);
		tree.root.left.right = new TreeNode(7);

		System.out.println("从上到下打印二叉树：");
		tree.levelOrder(tree.root);
		System.out.println();

		System.out.println("从底向上打印二叉树：");
		List<List<Integer>> list = tree.levelOrderBottom(tree.root);
		for (List<Integer> list2 : list) {
			for (Integer integer : list2) {
				System.out.print(integer + " ");
			}
		}
	}

	/**
	 * 验证满二叉树
	 * 
	 * @throws Exception
	 */
	@Test
	public void testIsFullTree() throws Exception {
		BinaryTree tree = new BinaryTree();
		tree.root = new TreeNode(1);
		tree.root.left = new TreeNode(2);
		tree.root.right = new TreeNode(3);
		tree.root.left.left = new TreeNode(4);
		// tree.root.left.right = new TreeNode(5);

		System.out.println(tree.isFullTree(tree.root));
	}

	/**
	 * 测试找出二叉树的最大节点
	 * 
	 * @throws Exception
	 */
	@Test
	public void testFindMax() throws Exception {
		BinaryTree tree = new BinaryTree();
		tree.root = new TreeNode(2);
		tree.root.left = new TreeNode(7);
		tree.root.right = new TreeNode(5);
		tree.root.left.right = new TreeNode(6);
		tree.root.left.right.left = new TreeNode(1);
		tree.root.left.right.right = new TreeNode(11);
		tree.root.right.right = new TreeNode(9);
		tree.root.right.right.left = new TreeNode(4);

		try {
			System.out.println(tree.findMax(tree.root).val);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	/**
	 * 二叉树的所有路径
	 * 
	 * @throws Exception
	 */
	@Test
	public void testBinaryTreePaths() throws Exception {
		BinaryTree tree = new BinaryTree();
		tree.root = new TreeNode(10);
		tree.root.left = new TreeNode(8);
		tree.root.right = new TreeNode(2);
		tree.root.left.left = new TreeNode(3);
		tree.root.left.right = new TreeNode(5);
		tree.root.right.left = new TreeNode(2);

		/* Let us test the built tree by printing Insorder traversal */
		System.out.println(Arrays.toString(tree.binaryTreePaths(tree.root).toArray()));
	}

	/**
	 * 测试两棵树是否等价
	 * 
	 * @throws Exception
	 */
	@Test
	public void testIdenticalTrees() throws Exception {
		BinaryTree tree1 = new BinaryTree();

		tree1.root = new TreeNode(1);
		tree1.root.left = new TreeNode(2);
		tree1.root.right = new TreeNode(3);
		tree1.root.left.left = new TreeNode(4);
		tree1.root.left.right = new TreeNode(5);

		BinaryTree tree2 = new BinaryTree();
		tree2.root = new TreeNode(1);
		tree2.root.left = new TreeNode(2);
		tree2.root.right = new TreeNode(3);
		tree2.root.left.left = new TreeNode(4);
		tree2.root.left.right = new TreeNode(5);

		if (tree1.identicalTrees(tree1.root, tree2.root))
			System.out.println("Both trees are identical");
		else
			System.out.println("Trees are not identical");
	}

	/**
	 * 测试不使用递归计算二叉树的最大高度
	 * 
	 * @throws Exception
	 */
	@Test
	public void testHeightWithIterative() throws Exception {
		BinaryTree tree = new BinaryTree();

		// Let us create a binary tree shown in above diagram
		tree.root = new TreeNode(1);
		tree.root.left = new TreeNode(2);
		tree.root.right = new TreeNode(3);
		tree.root.left.left = new TreeNode(4);
		tree.root.left.right = new TreeNode(5);
		System.out.println("Height of tree is " + tree.heightWithIterative(tree.root));
	}

	/**
	 * 测试寻找二叉树的公共祖先
	 * 
	 * @throws Exception
	 * 
	 */
	@Test
	public void testFindLCA() throws Exception {
		BinaryTree tree = new BinaryTree();
		tree.root = new TreeNode(1);
		tree.root.left = new TreeNode(2);
		tree.root.right = new TreeNode(3);
		tree.root.left.left = new TreeNode(4);
		tree.root.left.right = new TreeNode(5);
		tree.root.right.left = new TreeNode(6);
		tree.root.right.right = new TreeNode(7);

		// System.out.println("LCA(4, 5) = " + tree.findLCA(tree.root, 4, 5).val);
		// System.out.println("LCA(4, 6) = " + tree.findLCA(tree.root, 4, 6).val);
		// System.out.println("LCA(3, 4) = " + tree.findLCA(tree.root, 3, 4).val);
		System.out.println("LCA(2, 4) = " + tree.findLCA(tree.root, 2, 4).val);
	}

	/**
	 * 测试根据中序遍历和前序遍历构造二叉树
	 * 
	 * @throws Exception
	 */
	@Test
	public void testBuildTree() throws Exception {
		BinaryTree tree = new BinaryTree();
		char in[] = new char[] { 'D', 'B', 'E', 'A', 'F', 'C' };
		char pre[] = new char[] { 'A', 'B', 'D', 'E', 'C', 'F' };
		int len = in.length;
		TreeNode root = tree.buildTree(in, pre, 0, len - 1);

		// building the tree by printing inorder traversal
		System.out.println("Inorder traversal of constructed tree is : ");
		tree.printInorder(root);
	}
}
