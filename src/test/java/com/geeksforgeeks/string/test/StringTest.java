package com.geeksforgeeks.string.test;

import com.geeksforgeeks.string.StringExample;
import org.junit.Test;

public class StringTest {

    /**
     * 测试字符串反转
     *
     * @throws Exception
     */
    @Test
    public void testReverse() throws Exception {
        String str = "Geeks for Geeks";
        StringExample.reverse(str);
        System.out.println();
        StringExample.reverseWithRecursion(str);
    }

    /**
     * 测试字符串全排列
     *
     * @throws Exception
     */
    @Test
    public void testPermute() throws Exception {
        String str = "ABC";
        int n = str.length();
        StringExample.permute(str, 0, n - 1);
    }

    /**
     * 测试获取出现次数最多的字符串
     */
    @Test
    public void testGetMaxOccuringChar() {
        String str = "sample string";
        System.out.println("Max occurring character is " +
                StringExample.getMaxOccuringChar(str));
    }
}
