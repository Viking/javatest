package com.geeksforgeeks.linkedlist;

import org.junit.Test;

/**
 * 链表测试
 * 
 * @author wangpeng
 *
 */
public class LinkedListTest {

	/**
	 * 测试打印给定链表中间的元素
	 * 
	 * @throws Exception
	 */
	@Test
	public void testPrintMiddle() throws Exception {
		LinkedList llist = new LinkedList();
		for (int i = 5; i > 0; --i) {
			llist.push(i);
			llist.printList();
			llist.printMiddle();
		}
	}
}
